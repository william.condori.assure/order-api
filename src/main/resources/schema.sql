CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS orders
(
    id              UUID NOT NULL DEFAULT uuid_generate_v4(),
    customer_name   CHARACTER VARYING(50)  NOT NULL,
    email_address   CHARACTER VARYING(50)  NOT NULL,
    CONSTRAINT orders_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS addresses
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    address_line_1  CHARACTER VARYING(100) NOT NULL,
    address_line_2  CHARACTER VARYING(100) ,
    contact_name    CHARACTER VARYING(50)  NOT NULL,
    contact_phone   CHARACTER VARYING(20)  NOT NULL,
    state           CHARACTER VARYING(20)  NOT NULL,
    city            CHARACTER VARYING(20)  NOT NULL,
    zip_code        CHARACTER VARYING(10)  NOT NULL,
    country_code    CHARACTER VARYING(2)  NOT NULL,
    order_id        UUID NOT NULL,
    CONSTRAINT addresses_pkey PRIMARY KEY (id),
    CONSTRAINT fk_addresses_orders FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE IF NOT EXISTS line_items
(
    id          UUID NOT NULL DEFAULT uuid_generate_v4(),
    qty         INTEGER NOT NULL,
    product_id  UUID NOT NULL,
    order_id    UUID NOT NULL,
    CONSTRAINT line_items_pkey PRIMARY KEY (id),
    CONSTRAINT fk_line_items_orders FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);
