DELETE FROM line_items;
DELETE FROM addresses;
DELETE FROM orders;

INSERT INTO public.orders(id, customer_name, email_address)
VALUES ('e88a924f-26dc-4680-9cd2-ee455e2c55ae', 'Barry Peterson', 'mauris.sapien@aol.org');

INSERT INTO public.addresses(id, address_line_1, address_line_2, contact_name, contact_phone, state, city, zip_code, country_code, order_id)
VALUES ('88b2d707-4fd3-49da-9862-775cb109d4c9','653-7632 Vulputate Av.','Ap #887-6389 Facilisi. St.', 'Bevis Grant', '(350) 763-9250', 'Colorado', 'Aurora', '49275', 'US', 'e88a924f-26dc-4680-9cd2-ee455e2c55ae');

INSERT INTO public.line_items( qty, product_id, order_id)
VALUES ( 50, '952c077a-cd32-4d62-8c37-c23250cd4308', 'e88a924f-26dc-4680-9cd2-ee455e2c55ae');