package com.milankas.order_api.web.controller;

import com.milankas.order_api.domain.dto.OrderRequestDTO;
import com.milankas.order_api.domain.dto.OrderResponseDTO;
import com.milankas.order_api.domain.service.OrderService;
import com.milankas.order_api.utils.validation.ValidUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/orders")
@Validated
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/")
    public ResponseEntity<List<OrderResponseDTO>> getAll() {
        return ResponseEntity.ok(orderService.getAll());
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderResponseDTO> getOrder(@ValidUUID @PathVariable("orderId") String orderId) {
        return orderService.getOrder(orderId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/")
    public ResponseEntity<OrderResponseDTO> createOrder(@Valid @RequestBody OrderRequestDTO orderRequestDTO) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderService.createOrder(orderRequestDTO));
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity delete(@ValidUUID @PathVariable("orderId") String orderId) {
        if (orderService.deleteOrder(orderId)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/healthcheck")
    public ResponseEntity healthcheck(){
        HashMap<String, String> map = new HashMap<>();
        map.put("status", "Up");
        return ResponseEntity.ok(map);
    }
}
