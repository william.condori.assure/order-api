package com.milankas.order_api.domain.service;

import com.milankas.order_api.domain.dto.AddressRequestDTO;
import com.milankas.order_api.domain.dto.AddressResponseDTO;
import com.milankas.order_api.persistence.crud.AddressCrudRepository;
import com.milankas.order_api.persistence.entity.Address;
import com.milankas.order_api.persistence.mapper.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    private AddressCrudRepository addressRepository;

    @Autowired
    private AddressMapper mapper;

    public AddressResponseDTO save(AddressRequestDTO addressRequestDTO){
        Address address =  addressRepository.save(mapper.toAddress(addressRequestDTO));
        return mapper.toAddressResponseDTO(address);
    }

}
