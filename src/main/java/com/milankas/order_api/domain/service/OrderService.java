package com.milankas.order_api.domain.service;

import com.milankas.order_api.clients.ProductClientRest;
import com.milankas.order_api.domain.dto.*;
import com.milankas.order_api.persistence.crud.AddressCrudRepository;
import com.milankas.order_api.persistence.crud.LineItemCrudRepository;
import com.milankas.order_api.persistence.crud.OrderCrudRepository;
import com.milankas.order_api.persistence.entity.LineItem;
import com.milankas.order_api.persistence.entity.Order;
import com.milankas.order_api.persistence.mapper.AddressMapper;
import com.milankas.order_api.persistence.mapper.LineItemMapper;
import com.milankas.order_api.persistence.mapper.OrderMapper;
import com.milankas.order_api.web.exception.ApiRequestException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OrderService {

    @Autowired
    private OrderCrudRepository orderRepository;

    @Autowired
    private AddressCrudRepository addressRepository;

    @Autowired
    private LineItemCrudRepository lineItemRepository;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private LineItemMapper lineItemMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private ProductClientRest productClientRest;

    public List<OrderResponseDTO> getAll() {
        return orderMapper.toOrderResponseDTOList(
                orderRepository.findAll());
    }

    public Optional<OrderResponseDTO> getOrder(String orderId) {
        return orderRepository.findById(UUID.fromString(orderId))
                .map(order -> orderMapper.toOrderResponseDTO(order));
    }

    public OrderResponseDTO createOrder(OrderRequestDTO orderRequestDTO) {
        try {
            orderRequestDTO.getLineItems().stream().forEach(lineItemRequestDTO -> {
                productClientRest.getProduct(lineItemRequestDTO.getProductId()).getStatusCode();
            });

            Order order = orderRepository.save(orderMapper.toOrder(orderRequestDTO));

            AddressRequestDTO addressRequestDTO = orderRequestDTO.getShippingAddress();
            addressRequestDTO.setOrderId(order.getId());
            order.setAddress(addressRepository.save(addressMapper.toAddress(addressRequestDTO)));

            order.setLineItems(orderRequestDTO.getLineItems().stream().map(lineItemRequestDTO -> {
                lineItemRequestDTO.setOrderId(order.getId());
                return lineItemRepository.save(lineItemMapper.toLineItem(lineItemRequestDTO));
            }).collect(Collectors.toList()));

            return orderMapper.toOrderResponseDTO(order);
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                throw new ApiRequestException("productId doesn't exist");
            }
            throw new ApiRequestException("microservice product is not available");
        }
    }

    public boolean deleteOrder(String orderId) {
        return getOrder(orderId).map(orderResponseDTO -> {
            orderRepository.deleteById(UUID.fromString(orderId));
            return true;
        }).orElse(false);
    }

}
