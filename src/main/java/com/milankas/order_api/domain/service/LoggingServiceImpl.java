package com.milankas.order_api.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.milankas.order_api.domain.dto.log.LogDTO;
import com.milankas.order_api.domain.dto.log.LogMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Slf4j
@Component
public class LoggingServiceImpl implements LoggingService {

    @Value("${spring.application.name}")
    private String serviceName;

    @Autowired
    private RabbitTemplate template;

    @Value("${rabbitmq.exchange}")
    private String EXCHANGE;

    @Value("${rabbitmq.routing_key}")
    private String ROUTING_KEY;

    @Override
    public void logRequest(HttpServletRequest httpServletRequest, Object body) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            Map<String, String> parameters = buildParametersMap(httpServletRequest);

            LogMessageDTO logMessage = LogMessageDTO.builder()
                    .httpMethod(httpServletRequest.getMethod())
                    .path(httpServletRequest.getRequestURI())
                    .headers(buildHeadersMap(httpServletRequest))
                    .clientIp(httpServletRequest.getRemoteAddr())
                    .build();


            if (!parameters.isEmpty()) {
                logMessage.setParameters(parameters);
            }

            if (body != null) {
                logMessage.setResponse(body.toString());
            }

            LogDTO logDTO = new LogDTO();
            logDTO.setLevel("info");
            logDTO.setServiceName(serviceName);
            logDTO.setCreatedAt(new Date());
            logDTO.setMessage(objectMapper.writeValueAsString(logMessage));

            template.convertAndSend(EXCHANGE, ROUTING_KEY, logDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object body) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            LogMessageDTO logMessage = LogMessageDTO.builder()
                    .httpStatus(httpServletResponse.getStatus())
                    .httpMethod(httpServletRequest.getMethod())
                    .path(httpServletRequest.getRequestURI())
                    .headers(buildHeadersMap(httpServletResponse))
                    .response(body.toString())
                    .clientIp(httpServletRequest.getRemoteAddr())
                    .build();

            LogDTO logDTO = new LogDTO();
            logDTO.setServiceName(serviceName);
            logDTO.setCreatedAt(new Date());
            logDTO.setMessage(objectMapper.writeValueAsString(logMessage));
            logDTO.setLevel(httpServletResponse.getStatus() >= 400 ? "error" : "info");

            template.convertAndSend(EXCHANGE, ROUTING_KEY, logDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> buildParametersMap(HttpServletRequest httpServletRequest) {
        Map<String, String> resultMap = new HashMap<>();
        Enumeration<String> parameterNames = httpServletRequest.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String key = parameterNames.nextElement();
            String value = httpServletRequest.getParameter(key);
            resultMap.put(key, value);
        }

        return resultMap;
    }

    private Map<String, String> buildHeadersMap(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }

    private Map<String, String> buildHeadersMap(HttpServletResponse response) {
        Map<String, String> map = new HashMap<>();

        Collection<String> headerNames = response.getHeaderNames();
        for (String header : headerNames) {
            map.put(header, response.getHeader(header));
        }

        return map;
    }
}
