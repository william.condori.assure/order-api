package com.milankas.order_api.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.milankas.order_api.utils.validation.ValidUUID;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
public class LineItemRequestDTO {

    @NotNull
    @ValidUUID
    private String productId;
    @NotNull
    @Min(0)
    private Long qty;
    @JsonIgnore
    private UUID orderId;

}
