package com.milankas.order_api.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class AddressRequestDTO {

    @NotNull
    @Size(min = 3, max = 100)
    private String addressLine1;
    @Size(min = 3, max = 100)
    private String addressLine2;
    @NotNull
    @Size(min = 3, max = 50)
    private String contactName;
    @NotNull
    @Size(min = 10, max = 20)
    @Pattern(regexp = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$", message = "Invalid contact phone")
    private String contactPhoneNo;
    @NotNull
    @Size(min = 3, max = 20)
    private String state;
    @NotNull
    @Size(min = 3, max = 20)
    private String city;
    @NotNull
    @Size(min = 3, max = 20)
    private String zipCode;
    @NotNull
    @Pattern(regexp = "[A-Z]{2}", message = "Invalid country code")
    private String countryCode;
    @JsonIgnore
    private UUID orderId;

}
