package com.milankas.order_api.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class LineItemResponseDTO {

    private UUID id;
    private UUID productId;
    private Integer qty;

}
