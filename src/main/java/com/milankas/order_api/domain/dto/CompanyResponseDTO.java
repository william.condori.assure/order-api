package com.milankas.order_api.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyResponseDTO {

    private String id;
    private String name;
    private AddressCompanyResponseDTO address;

}