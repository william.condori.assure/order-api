package com.milankas.order_api.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponseDTO {

    private UUID id;
    private String customerName;
    private String emailAddress;
    private AddressResponseDTO shippingAddress;
    private List<LineItemResponseDTO> lineItems;

}
