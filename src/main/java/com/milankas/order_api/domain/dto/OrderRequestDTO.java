package com.milankas.order_api.domain.dto;

import com.milankas.order_api.web.exception.ApiRequestException;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class OrderRequestDTO {

    @NotNull
    private String customerName;
    @Email
    @NotNull
    private String emailAddress;
    @Valid
    @NotNull
    private AddressRequestDTO shippingAddress;
    @NotNull
    private List<@Valid LineItemRequestDTO> lineItems;
}
