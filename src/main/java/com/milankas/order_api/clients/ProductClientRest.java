package com.milankas.order_api.clients;

import com.milankas.order_api.domain.dto.ProductResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "microservice-product")
@RequestMapping("/v1/products")
public interface ProductClientRest {

    @GetMapping("/{productId}")
    ResponseEntity<ProductResponseDTO> getProduct(@PathVariable("productId") String productId);

}
