package com.milankas.order_api.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "line_items")
public class LineItem {

    @Id
    @GeneratedValue
    private UUID id;
    private Long qty;
    @Column(name = "product_id")
    private UUID productId;
    @Column(name = "order_id")
    private UUID orderId;
    @ManyToOne
    @JoinColumn(name = "order_id",referencedColumnName = "id", insertable = false, updatable = false)
    private Order order;
}
