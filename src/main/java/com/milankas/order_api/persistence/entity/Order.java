package com.milankas.order_api.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "email_address")
    private String emailAddress;
    @OneToMany(mappedBy = "order")
    private List<LineItem> lineItems;
    @OneToOne(mappedBy = "order")
    private Address address;

}
