package com.milankas.order_api.persistence.mapper;

import org.mapstruct.Named;

import java.util.UUID;

public class HelperMapper {
    @Named("stringToUUID")
    static UUID stringToUUID(String value) {
        return UUID.fromString(value);
    }

    @Named("UUIDtoString")
    static String UUIDtoString(UUID value) {
        return value.toString();
    }
}
