package com.milankas.order_api.persistence.mapper;

import com.milankas.order_api.domain.dto.AddressRequestDTO;
import com.milankas.order_api.domain.dto.AddressResponseDTO;
import com.milankas.order_api.persistence.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {HelperMapper.class})
public interface AddressMapper {

    AddressResponseDTO toAddressResponseDTO(Address address);

    Address toAddress(AddressRequestDTO addressRequestDTO);
}
