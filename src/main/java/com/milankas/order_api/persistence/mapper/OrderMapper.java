package com.milankas.order_api.persistence.mapper;

import com.milankas.order_api.domain.dto.OrderRequestDTO;
import com.milankas.order_api.domain.dto.OrderResponseDTO;
import com.milankas.order_api.persistence.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {HelperMapper.class, LineItemMapper.class, AddressMapper.class})
public interface OrderMapper {

    @Mapping(source = "address", target = "shippingAddress")
    OrderResponseDTO toOrderResponseDTO(Order order);

    List<OrderResponseDTO> toOrderResponseDTOList(List<Order> orders);

    @Mappings({
            @Mapping(target = "address", ignore = true),
            @Mapping(target = "lineItems", ignore = true)
    })
    Order toOrder(OrderRequestDTO orderRequestDTO);

}
