package com.milankas.order_api.persistence.mapper;

import com.milankas.order_api.domain.dto.LineItemRequestDTO;
import com.milankas.order_api.domain.dto.LineItemResponseDTO;
import com.milankas.order_api.persistence.entity.LineItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {HelperMapper.class})
public interface LineItemMapper {

    LineItemResponseDTO toLineItemResponseDTO(LineItem lineItem);

    @Mapping(target = "productId", qualifiedByName = "stringToUUID")
    LineItem toLineItem(LineItemRequestDTO lineItemRequestDTO);
}
