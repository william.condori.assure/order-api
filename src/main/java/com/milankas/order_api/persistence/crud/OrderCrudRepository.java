package com.milankas.order_api.persistence.crud;

import com.milankas.order_api.persistence.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderCrudRepository extends JpaRepository<Order, UUID> {
}
