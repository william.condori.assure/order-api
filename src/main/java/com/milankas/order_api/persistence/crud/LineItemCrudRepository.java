package com.milankas.order_api.persistence.crud;

import com.milankas.order_api.persistence.entity.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LineItemCrudRepository extends JpaRepository<LineItem, UUID> {
}
