package com.milankas.order_api.web.controller;

import com.milankas.order_api.domain.dto.OrderResponseDTO;
import com.milankas.order_api.domain.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService service;

    @Test
    public void givenOrders_whenGetAll_thenReturnOk() throws Exception {

        List<OrderResponseDTO> orders = Arrays.asList(new OrderResponseDTO());

        when(service.getAll()).thenReturn(orders);

        this.mockMvc.perform(get("/orders/"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGetOrderByIdWithBadUUI_thenReturnNotFound() throws Exception {

        List<OrderResponseDTO> orders = Arrays.asList(new OrderResponseDTO());

        when(service.getAll()).thenReturn(orders);

        this.mockMvc.perform(get("/orders/629fa58f-ea78-4a9e-a662"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetOrderById_thenReturnOk() throws Exception {
        OrderResponseDTO orderResponseDTO = new OrderResponseDTO();

        when(service.getOrder(anyString())).thenReturn(Optional.of(orderResponseDTO));

        this.mockMvc.perform(get("/orders/629fa58f-ea78-4a9e-a662-a9c53298cffe"))
                .andExpect(status().isOk());
    }
}